<?php
/* 
  Copyright 2016 SamuelBF

  Ce fichier fait partie du logiciel Prefmon, logiciel libre placé sous la 
  license GNU General Public License version 3. Vous devriez avoir reçu une
  copie de la license avec ce logiciel. Si ça n'est pas le cas, vous pouvez 
  la trouver en ligne à l'adresse : <http://www.gnu.org/licenses/>.
*/

/* Log : enregistrement (et affichage, si demandé) des messages d'erreur et 
   d'avertissement générés par les scripts PHP */

class Log {
  private $fichier, $niveau, $stdoutput, $formatSortie;

  # Niveaux d'enregistrement :
  const DEBUG = 3;
  const INFORMATION = 2;
  const AVERTISSEMENT = 1;
  const ERREUR = 0;

  # Format de sortie :
  const TERM = 0;
  const HTML = 1;
  
  public function __construct($niveau, $nom_fichier, $stdoutput = false, $formatSortie = Log::TERM) {
    
    # Création du répertoire si besoin :
    if(!is_dir(dirname($nom_fichier)))
      mkdir(dirname($nom_fichier));

    # Ouverture du fichier de logs :
    $this->fichier = fopen($nom_fichier, 'a');
    $this->niveau = $niveau;
    $this->stdoutput = $stdoutput;
    $this->formatSortie = $formatSortie;
  }

  public function __destruct() {
    fclose($this->fichier);
  }

  # Fonction de transformation constante -> str (pour écriture dans le fichier de logs)
  private function niveauStr($niveau) {
    switch($niveau) {
    case $this::DEBUG:         return "[debug ]"; break;
    case $this::INFORMATION:   return "[inform]"; break;
    case $this::AVERTISSEMENT: return "[avert.]"; break;
    case $this::ERREUR:        return "[erreur]"; break;
    }
  }

  # Ajouter un formatage à la sortie standard et/ou HTML :
  private function souligner($niveau, $str)  {
    switch($this->formatSortie) {
    case $this::TERM:
      switch($niveau) {
      case $this::DEBUG:         return "\e[2m".$str."\e[0m"; break;
      case $this::INFORMATION:   return $str; break;
      case $this::AVERTISSEMENT: return "\e[4m".$str."\e[0m"; break;
      case $this::ERREUR:        return "\e[1m".$str."\e[0m"; break;
      } break;
    case $this::HTML:
      switch($niveau) {
      case $this::DEBUG:         return "<small>".$str."</small><br/>"; break;
      case $this::INFORMATION:   return $str."<br/>"; break;
      case $this::AVERTISSEMENT: return "<em>".$str."</em><br/>"; break;
      case $this::ERREUR:        return "<b>".$str."</b><br/>"; break;
      } break;
    }
  }
  
  # Enregistrement d'un message (si son niveau justifie l'affichage) :
  public function message($niveau, $message) {
    if($niveau <= $this->niveau) {
      $str = date('[H:i:s] ') . $this->niveauStr($niveau) . ' ' . $message . "\n";
      fwrite($this->fichier, $str);
      if($this->stdoutput)
        echo $this->souligner($niveau, $str);
    }
  }
  
}

?>