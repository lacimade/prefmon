# Présentation du projet

Prefmon est un logiciel de surveillance des délais de rendez-vous pour les
démarches administratives en préfecture : il visite les sites des préfectures
lorsqu'on lui demande puis enregistre les délais dans une base de données.

# Accès aux données

Une version du logiciel collecte actuellement des données. Celles-ci sont
publiées en ligne à l'adresse :
[aguichetsfermes.lacimade.org/](https://aguichetsfermes.lacimade.org/)

# Utiliser le logiciel sur un hébergement Web classique

## Pré-requis

Ce logiciel est pensé pour être déployé sur un serveur comprenant :
 - Une base de données MySQL ou MariaDB
 - La possibilité d'éxécuter des scripts PHP
 - La possibilité d'écrire des fichiers
 - La possibilité d'exécuter des scripts régulièrement (si on souhaite
   automatiser les sondages)

## Installation

1. Récupérez le code :
    ```sh
    $ git clone https://framagit.org/lacimade/prefmon.git
    ```
1. Créez votre base de données
1. Créez la structure de la base de données en y exécutant les commandes SQL
   contenues dans le fichier [base.sql](base.sql) (la procédure 'Couleur' n'est pas
   obligatoire)
1. Créez une tâche régulière ('cron') exécutant le script PHP :
   [effectuer_sondage.php](effectuer_sondage.php)

## Configuration

1. Créez le fichier configuration.ini à partir du modèle [configuration.ini.exemple](configuration.ini.exemple) en
   indiquant les informations pour se connecter à la base de données et le répertoire où seront stockées les
   archives.
1. Pour ajouter une démarche à surveiller, modifiez le fichier
   [sondages.ini](sondages.ini).

# Utiliser le logiciel via Docker

## Pré-requis

Ce logiciel est pensé pour être déployé sur un serveur comprenant :
 - Un démon docker correctement configuré
 - La possibilité d'écrire des fichiers

## Installation

1. Récupérez le code :
     ```sh
     $ git clone https://framagit.org/lacimade/prefmon.git
     ```
1. Créer l'image Docker
     ```sh
     $ docker-compose build
     ```

### Configuration

1. Créez le fichier .env à partir du modèle [.env.exemple](.env.exemple) en indiquant les informations pour se connecter à la base de données, les informations pour le proxy `NETWORK_PROXY` ou des paramètres particuliers concernant les sondages `SONDAGE_PARAMS`
1. Créez une tâche régulière ([crontab](https://github.com/dubiousjim/dcron/blob/master/crontab.markdown)) à partir du modèle [prefmon.cron.exemple](prefmon.cron.exemple) en ajustant la fréquence
1. Pour ajouter une démarche à surveiller, modifiez le fichier
   [sondages.ini](sondages.ini).

### Lancement
1. Lancez les services :
     ```sh
     $ docker-compose up --detach
     ```
